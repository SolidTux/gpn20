\documentclass{beamer}

\usetheme{solidtux}

\usepackage{pgfplots}
\usepackage{nicefrac}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{physics}
\usepackage{tikz}
\usepackage[
    exponent-product=\cdot,
]{siunitx}
\usepackage{mwe}
\usepackage[
    european,
]{circuitikz}
\RequirePackage[
    style=phys,
    maxnames=1,
]{biblatex}

\addbibresource{../literature.bib}
\AtEveryCitekey{\clearfield{title}}
\AtEveryCitekey{\clearfield{page}}

\usetikzlibrary{positioning}
\usetikzlibrary{quantikz}
\usepgfplotslibrary{fillbetween}

\makeatletter
\newcommand{\blfootnote}[1]{\gdef\@thefnmark{}\@footnotetext{\tiny #1}}
\makeatother
\newcommand{\id}{\ensuremath{\mathbb{1}}}

\title{Einführung in Quantencomputer Programmierung}
\author{Daniel Hauck}
\date{20.05.2022}

\begin{document}
\maketitle

\section{Grundlagen}
\subsection{Braket}
\begin{sframe}
    \begin{itemize}
        \item "bracket" $\to$ "bra"-"ket"
        \item Vektor \begin{align*}
                  \vec{a}         & = \mqty(a_0                           \\a_1) \equiv \ket{a} \\
                  \vec{a}^\dagger & = \mqty(a_0^* & a_1^*) \equiv \bra{a} \\
              \end{align*}
        \item Skalarprodukt \begin{align*}
                  \vec{a}^\dagger\vec{b}
                  = a_0^*b_0 + a_1^*b_1 \equiv \braket{a}{b}
              \end{align*}
    \end{itemize}
\end{sframe}
\subsection{Quantum?}
\begin{sframe}
    \begin{block}{Klassisches Bit}
        0 oder 1, \emph{deterministisch}
    \end{block}
    \visible<2>{\begin{block}{Qubit}
            mit bestimmter Wahrscheinlichkeit 0 oder 1, \emph{probabilistisch}
        \end{block}}
\end{sframe}
\subsection{Qubit}
\begin{sframe}
    \begin{itemize}
        \item Beschreibung durch Vektor
        \item 100\% "0": $\mqty(1\\0)\equiv \ket{0}$, 100\% "1": $\mqty(0\\1)\equiv \ket{1}$
        \item allgemeiner Zustand ($\alpha$, $\beta$ komplex): \begin{align*}
                  \ket{\psi} & = \mqty(\alpha                 \\\beta)
                  = \alpha\mqty(1                             \\0) + \beta\mqty(0\\1) \\
                  & = \alpha\ket{0} + \beta\ket{1}
              \end{align*}
        \item Normierung:
              \begin{align*}
                  \abs{\vec{\psi}}^2 =
                  \abs{\alpha}^2 + \abs{\beta}^2 = 1
              \end{align*}
    \end{itemize}
\end{sframe}
\begin{sframe}
    \centering
    \begin{block}{Zustand}
        \begin{align*}
            \ket{\psi} = \alpha\ket{0} + \beta\ket{1}
        \end{align*}
    \end{block}
    \visible<2->{\begin{quantikz}
            \lstick{$\ket{\psi}$} & \meter{}\qw
        \end{quantikz}}
    \visible<3->{\begin{itemize}
            \item Wahrscheinlichkeit, "0" bzw. "1" zu messen
                  \begin{align*}
                      P(0) & = \abs{\braket{0}{\psi}}^2 = \abs{\alpha}^2 \\
                      P(1) & = \abs{\braket{1}{\psi}}^2 = \abs{\beta}^2
                  \end{align*}
        \end{itemize}}
\end{sframe}
\subsection{Mehrere Qubits}
\begin{sframe}
    \begin{block}{2 Qubits}
        \begin{align*}
            \ket{\psi} & =
            \left(\alpha_0\ket{0}+\beta_0\ket{0}\right)\otimes
            \left(\alpha_1\ket{0}+\beta_1\ket{0}\right)                                \\[-3ex]
            & = a_{00}\ket{00}+a_{01}\ket{01}+a_{10}\ket{10}+a_{11}\ket{11}
            = \mqty(a_{00}                                                             \\a_{01}\\a_{10}\\a_{11})
        \end{align*}
    \end{block}
\end{sframe}
\subsection{Einschub: Kroneckerprodukt}
\begin{sframe}
    \begin{align*}
        & \mqty(
        a_{00}       & a_{01}       & \cdots   \\
        a_{10}       & a_{11}       & \cdots   \\
        \vdots       & \vdots       & \ddots
        ) \otimes
        \mqty(
        b_{00}       & b_{01}       & \cdots   \\
        b_{10}       & b_{11}       & \cdots   \\
        \vdots       & \vdots       & \ddots
        )                                      \\
        =            & \mqty(
        a_{00}b_{00} & a_{00}b_{01} & \cdots &
        a_{01}b_{00} & a_{01}b_{01} & \cdots
        \\
        a_{00}b_{10} & a_{00}b_{11} & \cdots &
        a_{01}b_{10} & a_{01}b_{11} & \cdots
        \\
        \vdots       & \vdots       & \ddots &
        \vdots       & \vdots       & \ddots
        \\
        a_{10}b_{00} & a_{10}b_{01} & \cdots &
        a_{11}b_{00} & a_{11}b_{01} & \cdots
        \\
        a_{10}b_{10} & a_{10}b_{11} & \cdots &
        a_{11}b_{10} & a_{11}b_{11} & \cdots
        \\
        \vdots       & \vdots       & \ddots &
        \vdots       & \vdots       & \ddots
        )
    \end{align*}
\end{sframe}
\begin{sframe}
    \begin{align*}
        \ket{00} & \equiv\ket{0}\otimes\ket{0}=\mqty(1 \\0)\otimes\mqty(1\\0)=\mqty(1&0&0&0)^\mathsf{T} \\
        \ket{01} & \equiv\ket{0}\otimes\ket{1}=\mqty(1 \\0)\otimes\mqty(0\\1)=\mqty(0&1&0&0)^\mathsf{T} \\
        \ket{10} & \equiv\ket{1}\otimes\ket{0}=\mqty(0 \\1)\otimes\mqty(1\\0)=\mqty(0&0&1&0)^\mathsf{T} \\
        \ket{11} & \equiv\ket{1}\otimes\ket{1}=\mqty(0 \\1)\otimes\mqty(0\\1)=\mqty(0&0&0&1)^\mathsf{T}
    \end{align*}
\end{sframe}
\subsection{Gates}
\begin{sframe}
    \begin{block}{Ein Qubit}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x}$} & \gate{G} \only<2>{&\gate{F}} & \rstick{$\ket{y}$}\qw
        \end{quantikz}
        \begin{align*}
            \ket{y} & = \only<2>{F}G\ket{x}
        \end{align*}
        \only<2>{$F$,}$G$: Gate ($2\times2$ Matrix) $\Rightarrow$ Matrixmultiplikation!
        \\
        \visible<3>{wenn keine Messungen o.ä. vorkommen, unitär $\Rightarrow$ nur reversible Operationen}
    \end{block}
\end{sframe}
\begin{sframe}
    \begin{block}{Mehrere Qubits}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x_0}$} & \gate{G} & \gate[wires=2]{E} & \gate{D} & \rstick{$\ket{y_0}$}\qw \\
            \lstick{$\ket{x_1}$} & \gate{F} &  & \qw & \rstick{$\ket{y_1}$}\qw \\
        \end{quantikz}
        \begin{align*}
            \ket{y_1y_0} & = \left(\id_2\otimes D\right)E\left(F\otimes G\right)\ket{x_1x_0}
        \end{align*}
        \begin{tabular}{rl}
            $\id_n$:       & $n\times n$ Einheitsmatrix \\
            $D$, $F$, $G$: & $2\times2$ Matrix          \\
            $E$:           & $4\times4$ Matrix
        \end{tabular}
    \end{block}
\end{sframe}
\begin{sframe}
    \begin{block}{Beispiel: $X$ Gate\only<3>{ (NOT)}}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x}$} & \gate{X} & \rstick{$\ket{y}$}\qw
        \end{quantikz} \raisebox{-0.5ex}{oder}
        \begin{quantikz}
            \lstick{$\ket{x}$} & \targ{} & \rstick{$\ket{y}$}\qw
        \end{quantikz}
        \visible<2->{\begin{align*}
                X & = \mqty(0 & 1 \\1&0)
            \end{align*}
            \begin{align*}
                X\ket{0} & = \only<2>{?}\only<3>{\ket{1}} \\
                X\ket{1} & = \only<2>{?}\only<3>{\ket{0}}
            \end{align*}}
    \end{block}
\end{sframe}
\begin{sframe}
    \begin{block}{Beispiel: $CX$ Gate (CNOT/controlled NOT)}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x_0}$} & \targ{} & \rstick{$\ket{y_0}$}\qw \\
            \lstick{$\ket{x_1}$} & \ctrl{-1} & \rstick{$\ket{y_1}$}\qw
        \end{quantikz}
        \visible<2->{\begin{align*}
                CX & = \mqty(
                1  & 0        & 0 & 0 \\
                0  & 1        & 0 & 0 \\
                0  & 0        & 0 & 1 \\
                0  & 0        & 1 & 0
                )
            \end{align*}}
        \vspace{-3ex}
        \visible<3>{\begin{align*}
                CX\ket{00} & = \ket{00} &
                CX\ket{01} & = \ket{01}   \\
                CX\ket{10} & = \ket{11} &
                CX\ket{11} & = \ket{10}
            \end{align*}}
        \vspace{-1ex}
    \end{block}
\end{sframe}
\begin{frame}[standout]
    \huge\bfseries
    Welche anderen Gates gibt es? \\
    Und was machen diese?
\end{frame}
\subsection{Einige weitere wichtige Gates \dots}
\begin{sframe}
    \centering
    \begin{tabular}{cm{0.2\textwidth}m{0.4\textwidth}}
        \toprule
        Gate                       & Matrix & Funktion \\
        \midrule                                       \\[-6ex]
        \begin{tikzcd}
            & \gate{H} & \qw
        \end{tikzcd} &
        \begin{align*}
            \frac{1}{\sqrt{2}}\mqty(1 & 1                 \\1&-1)
        \end{align*} &
        \begin{align*}
            \ket{0} &\to \frac{1}{\sqrt{2}}\left(\ket{0}+\ket{1}\right) \\
            \ket{1} &\to \frac{1}{\sqrt{2}}\left(\ket{0}-\ket{1}\right)
        \end{align*}                     \\[-6ex]
        \begin{tikzcd}
            & \gate{Z} & \qw
        \end{tikzcd} &
        \begin{align*}
            \mqty(1 & 0                 \\0&-1)
        \end{align*} &
        \begin{align*}
            \ket{0} &\to \ket{0} \\
            \ket{1} &\to -\ket{1}
        \end{align*}                     \\[-8ex]
        \begin{tikzcd}
            & \gate{T} & \qw
        \end{tikzcd} &
        \begin{align*}
            \mqty(1 & 0                 \\0&\mathsf{e}^{\mathsf{i}\nicefrac{\pi}{4}})
        \end{align*} &
        \begin{align*}
            \ket{0} &\to \ket{0} \\
            \ket{1} &\to \mathsf{e}^{\mathsf{i}\frac{\pi}{4}}\ket{1}
        \end{align*}                     \\[-2ex]
        \bottomrule
    \end{tabular}
\end{sframe}
\subsection{Logikgates}
\begin{sframe}
    \begin{block}{XOR}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x_0}$} & \ctrl{1} & \rstick{$\ket{x_0}$}\qw \\
            \lstick{$\ket{x_1}$} & \targ{} & \rstick{$\ket{y}$}\qw
        \end{quantikz}
        \begin{align*}
            y = x_0\oplus x_1
        \end{align*}
    \end{block}
\end{sframe}
\begin{sframe}
    \begin{block}{\visible<2->{AND}}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x_0}$} & \ctrl{2} & \rstick{$\ket{x_0}$}\qw \\
            \lstick{$\ket{x_1}$} & \ctrl{1} & \rstick{$\ket{x_1}$}\qw \\
            \lstick{%
                \only<1-2>{$\ket{0}$}%
                \only<3>{$\ket{x_2}$}%
            } & \targ{} & \rstick{$\ket{y}$}\qw
        \end{quantikz}
        \visible<2->{\begin{align*}
                \only<2>{y = x_0\land x_1}%
                \only<3>{y = x_2\oplus\left(x_0\land x_1\right)}
            \end{align*}}
    \end{block}
\end{sframe}
\begin{sframe}
    \begin{block}{\visible<2->{OR}}
        \centering
        \begin{quantikz}
            \lstick{$\ket{x_0}$} & \ctrl{2} & \ctrl{2} & \qw      & \rstick{$\ket{x_0}$}\qw \\
            \lstick{$\ket{x_1}$} & \ctrl{1} & \qw & \ctrl{1} & \rstick{$\ket{x_1}$}\qw \\
            \lstick{%
                \only<1-2>{$\ket{0}$}%
                \only<3>{$\ket{x_2}$}%
            } & \targ{}& \targ{} & \targ{} & \rstick{$\ket{y}$}\qw
        \end{quantikz}
        \visible<2->{\begin{align*}
                \only<2>{y = x_0\lor x_1}%
                \only<3>{y = x_2\oplus\left(x_0\land x_1\right)}
            \end{align*}}
    \end{block}
\end{sframe}
\subsection{Beispiel}
\begin{sframe}
    \centering
    \begin{align*}
        f(x_2x_1x_0) &= \left(x_0\oplus x_1\right)\land\left(x_1\land x_2\right) \\
        \ket{x_2x_1x_0,\dots,0} &\to \ket{x_2x_1x_0,\dots,f(x_2x_1x_0)} \\
        \text{bzw. }
        \ket{x_2x_1x_0,\dots,y} &\to \ket{x_2x_1x_0,\dots,f(x_2x_1x_0)\oplus y}
    \end{align*}
    \visible<2>{\begin{quantikz}
            \lstick{$\ket{x_0}$} & \ctrl{3} & \qw      & \qw      & \qw      & \rstick{$\ket{x_0}$} \qw \\
            \lstick{$\ket{x_1}$} & \qw      & \ctrl{2} & \ctrl{3} & \qw      & \rstick{$\ket{x_1}$} \qw \\
            \lstick{$\ket{x_2}$} & \qw      & \qw      & \ctrl{2} & \qw      & \rstick{$\ket{x_2}$} \qw \\
            \lstick{$\ket{a_0}$} & \targ{}  & \targ{}  & \qw      & \ctrl{2} & \rstick{$\ket{a_0}$} \qw \\
            \lstick{$\ket{a_1}$} & \qw      & \qw      & \targ{}  & \ctrl{1} & \rstick{$\ket{a_1}$} \qw \\
            \lstick{$\ket{y}$}   & \qw      & \qw      & \qw      & \targ{}  & \rstick{$\ket{f(x_2x_1x_0)\oplus y}$} \qw
        \end{quantikz}}
\end{sframe}
\subsection{Aufgabe}
\begin{sframe}
    \centering
    \begin{block}{$2\times2$ "Sudoku"}
        \centering
        \begin{tabular}{|c|c|}
            \hline
            $x_0$ & $x_1$ \\
            \hline
            $x_2$ & $x_3$ \\
            \hline
        \end{tabular}

        Kein doppelter Wert in jeder Zeile und Spalte.
    \end{block}
    \Large\bfseries
    Erstelle eine Schaltung, die Lösungen überprüft.
\end{sframe}
\subsection{Tools}
\begin{sframe}
    \begin{itemize}
        \item Quirk: \url{https://algassert.com/quirk}\begin{itemize}
                  \item graphisch
                  \item keine Registrierung notwendig
              \end{itemize}
        \item IBM-Q: \url{https://quantum-computing.ibm.com}\begin{itemize}
                  \item graphisch oder Jupyter Notebooks mit Qiskit
                  \item IBM Account notwendig
              \end{itemize}
        \item Qiskit: \url{https://qiskit.org/}\begin{itemize}
                  \item Python Bibliothen
                  \item keine Registrierung notwendig
                  \item optional Anbindung an IBM-Q
              \end{itemize}
        \item Circ \dots
    \end{itemize}
\end{sframe}
\subsection{Beispiellösung}
\begin{sframe}
    \centering
    Link zu Quirk: \href{https://algassert.com/quirk\#circuit=\%7B\%22cols\%22\%3A\%5B\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C1\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C1\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%5D\%7D}{\textcolor{TolDarkPink}{hier klicken}}

    \begin{quantikz}
        \lstick{$\ket{x_0}$} & \ctrl{4} & \qw & \qw & \qw & \ctrl{6} & \qw & \qw & \qw & \qw & \rstick{$\ket{x_0}$}\qw \\
        \lstick{$\ket{x_1}$} & \qw & \ctrl{3} & \qw & \qw & \qw & \qw & \ctrl{6} & \qw & \qw & \rstick{$\ket{x_1}$}\qw \\
        \lstick{$\ket{x_2}$} & \qw & \qw & \ctrl{3} & \qw & \qw & \ctrl{4} & \qw & \qw & \qw & \rstick{$\ket{x_2}$}\qw \\
        \lstick{$\ket{x_3}$} & \qw & \qw & \qw & \ctrl{2} & \qw & \qw & \qw & \ctrl{4} & \qw & \rstick{$\ket{x_3}$}\qw \\
        \lstick{$\ket{a_0}$} & \targ{} & \targ{} & \qw & \qw & \qw & \qw & \qw & \qw & \ctrl{4} & \rstick{$\ket{b_0}$}\qw \\
        \lstick{$\ket{a_1}$} & \qw & \qw & \targ{} & \targ{} & \qw & \qw & \qw & \qw & \ctrl{3} & \rstick{$\ket{b_1}$}\qw \\
        \lstick{$\ket{a_2}$} & \qw & \qw & \qw & \qw & \targ{} & \targ{} & \qw & \qw & \ctrl{2} & \rstick{$\ket{b_2}$}\qw \\
        \lstick{$\ket{a_3}$} & \qw & \qw & \qw & \qw & \qw & \qw & \targ{} & \targ{} & \ctrl{1} & \rstick{$\ket{b_3}$}\qw \\
        \lstick{$\ket{y}$} & \qw & \qw & \qw & \qw & \qw & \qw & \qw & \qw & \targ{} & \rstick{output}\qw \\
    \end{quantikz}
\end{sframe}
\subsection{Weiterführend}
\begin{sframe}
    \begin{itemize}
        \item Lösung mit weniger Qubits über algebraische Normalform
              \begin{itemize}
                  \item Link zu Beschreibung: \href{https://cstheory.stackexchange.com/a/51471}{\textcolor{TolDarkPink}{hier klicken}}
                  \item Link zu Quirk: \href{https://algassert.com/quirk\#circuit=\%7B\%22cols\%22\%3A\%5B\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%5D\%7D}{\textcolor{TolDarkPink}{hier klicken}}
              \end{itemize}
        \item Grover
              \begin{itemize}
                  \item Algorithmus zum Finden der Lösungen (statt nur Überprüfen wie bisher)
                  \item Link zu Quirk: \href{https://algassert.com/quirk\#circuit'\%7B\%22cols\%22\%3A\%5B\%5B\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C1\%2C\%22H\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22\%E2\%80\%A2\%22\%2C\%22X\%22\%5D\%2C\%5B\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%5D\%2C\%5B\%22X\%22\%2C\%22X\%22\%2C\%22X\%22\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C\%22H\%22\%5D\%2C\%5B\%22\%E2\%80\%A2\%22\%2C1\%2C1\%2C\%22X\%22\%5D\%2C\%5B1\%2C1\%2C1\%2C\%22H\%22\%5D\%2C\%5B\%22X\%22\%2C\%22X\%22\%2C\%22X\%22\%2C\%22X\%22\%5D\%2C\%5B\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%2C\%22H\%22\%5D\%2C\%5B\%22Chance4\%22\%5D\%5D\%7D}{\textcolor{TolDarkPink}{hier klicken}}
                  \item Link zu Tutorial: \href{https://qiskit.org/textbook/ch-algorithms/grover.html}{\textcolor{TolDarkPink}{hier klicken}}
              \end{itemize}
    \end{itemize}
\end{sframe}
\end{document}