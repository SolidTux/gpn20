\documentclass{beamer}

\usetheme{solidtux}

\usepackage{mhchem}
\usepackage{pgfplots}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{physics}
\usepackage[
    exponent-product=\cdot,
]{siunitx}
\usepackage{mwe}
\usepackage[
    european,
]{circuitikz}
\RequirePackage[
    style=phys,
    maxnames=1,
]{biblatex}

\addbibresource{../literature.bib}
\AtEveryCitekey{\clearfield{title}}
\AtEveryCitekey{\clearfield{page}}
\AtEveryCitekey{\clearfield{pages}}
\AtEveryCitekey{\clearfield{doi}}

\usetikzlibrary{positioning}
\usepgfplotslibrary{fillbetween}

\makeatletter
\newcommand{\blfootnote}[1]{\gdef\@thefnmark{}\@footnotetext{\tiny #1}}
\makeatother
\newcommand{\tc}{\ensuremath{T_\mathsf{c}}}

\title{Was ist denn eigentlich ein Supraleiter?}
\author{Daniel Hauck}
\institute{GPN20}
\date{20.05.2022}

% \renewcommand{\mathrm}[1]{\mathsf{#1}}
\begin{document}
\maketitle

\section{Entdeckung}
\subsection{Entdeckung}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \begin{itemize}
                \item Widerstandsmessung an Quecksilber bei tiefen Temperaturen
                \item Sprung bei $\tc\approx\SI{4.2}{K}$
            \end{itemize}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{tikzpicture}[
                    every node/.style={inner sep=0, outer sep=0}
                ]
                \node (img) {\includegraphics[width=0.7\linewidth]{fig/Superconductivity_1911.png}};
                \node[anchor=north, below=0 of img] {$T$ (K)};
                \node[anchor=east, left=0 of img] {$R$ ($\Omega$)};
            \end{tikzpicture}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{KamerlinghOnnes:1911}}
\end{sframe}

\section{Grundlagen}
\subsection{Leiter?}
\begin{sframe}
    \centering
    \begin{block}{Elektrischer Widerstand}
        \centering
        \begin{align*}
            U & = RI
        \end{align*}
        \only<1>{$\Rightarrow$ Wie viel Strom fließt bei einer angelegten Spannung?}
        \only<2>{\bfseries $\Rightarrow$ Wie viel Spannung fällt bei einem angelegten Strom ab?}
    \end{block}
    \visible<2>{\begin{circuitikz}
            \draw[
                thick, color=mDarkTeal
            ] (0,0) to[
                short,i=$I$, color=mDarkTeal
            ] (2,0) to (2,1.5) to[
                voltmeter, l=$U$, color=mDarkTeal
            ] (6,1.5) to[
                color=mDarkTeal
            ] (6,0) to[
                color=mDarkTeal
            ] (8,0);
            \draw[
                thick, color=mDarkTeal
            ] (2,0) to[R, *-*, l=$R$] (6,0);
        \end{circuitikz}}
\end{sframe}
\subsection{Magnetfelder}
\begin{sframe}
    \centering
    \begin{block}{Magnetisierung}
        Felder in einem Material
        \begin{align*}
            \vec{B} = \mu_0 \left(\vec{H} + \vec{M}\right) \quad
            \vec{M} = \chi \vec{H}
        \end{align*}
        \begin{center}
            $\vec{H}$: Magnetfeld,
            $\vec{B}$: magnetische Flussdichte,\\
            $\vec{M}$: Magnetisierung,
            $\chi$: Suszeptibilität
        \end{center}
    \end{block}
    Ferromagnet: $\chi\gg1$,
    Paramagnet: $\chi>0$, \\
    Diamagnet: $\chi<0$,
    \dots
\end{sframe}
\subsection{Magnetfelder}
\begin{sframe}
    \begin{block}{Magnetischer Fluss}
        \begin{align*}
            \Phi & = \int_A\vec{B}\cdot\dd{\vec{A}}
        \end{align*}
        \begin{center}
            $\Phi$: magnetischer Fluss,
            $\vec{B}$: magnetische Flussdichte,\\
            $A$: Fläche
        \end{center}
        \visible<2->{Bei konstantem $\vec{B}$
            \begin{align*}
                \Phi & = B_\perp A
            \end{align*}}
    \end{block}
\end{sframe}

\section{Supraleiter}
\subsection{Grundlegende Eigenschaften}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \begin{itemize}
                \item Widerstand $R=0$
                \item idealer Diamagnet $\vec{M}=-\vec{H}$, $\chi=-1$
            \end{itemize}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{tikzpicture}
                \begin{axis}[
                        width=0.9\linewidth,
                        height=0.8\textheight,
                        domain=0:2,
                        xlabel=$T$,
                        ylabel=$R$,
                        xtick={0,1},
                        ytick={0,1},
                        xticklabels={0,$T_\mathsf{c}$},
                        yticklabels={0,$R_0$},
                    ]
                    \addplot [thick,color=mDarkTeal,domain=1:2] {0.5*x^2+1};
                    \addplot [thick,color=mDarkTeal,domain=0:1,dashed] {0.5*x^2+1};
                    \addplot [thick,color=mDarkTeal] coordinates {
                            (0,0)
                            (1,0)
                            (1,1.5)
                        };
                \end{axis}
            \end{tikzpicture}
        \end{column}
    \end{columns}
\end{sframe}
\subsection{Phasendiagramm}
\begin{sframe}
    \begin{flushright}
        \only<1>{\begin{tikzpicture}[
                    text=mDarkTeal
                ]
                \begin{axis}[
                        width=\textwidth,
                        height=0.7\textwidth,
                        xlabel=$T$,
                        ylabel=$B$,
                        xtick={1},
                        xticklabels={\tc},
                        extra x ticks={0},
                        extra x tick style={draw=none,tick label style={anchor={north east}}},
                        ytick={1},
                        yticklabels={$B_\mathsf{c}$},
                        xtick style={draw=none},
                        ytick style={draw=none},
                        axis lines=middle,
                        hide obscured x ticks=false,
                        xmax=1.2,
                        ymin=0,
                        ymax=1.2,
                    ]
                    \addplot [color=mDarkTeal,name path=f,domain=0:1] {1-x^2};
                    \path[name path=axis] (axis cs:0,0) -- (axis cs:1,0);
                    \addplot[color=mDarkTeal,opacity=0.3] fill between[of=f and axis];
                    \node [anchor=center,align=center] at (axis cs:0.35,0.4) {Supraleiter\\(Meißner-Phase)};
                    \node [anchor=center,align=center] at (axis cs:0.8,0.8) {Normalleiter};
                \end{axis}
            \end{tikzpicture}}%
        \only<2>{\begin{tikzpicture}[
                    text=mDarkTeal
                ]
                \begin{axis}[
                        width=\textwidth,
                        height=0.7\textwidth,
                        xlabel=$T$,
                        ylabel=$B$,
                        xtick={1},
                        xticklabels={\tc},
                        extra x ticks={0},
                        extra x tick style={draw=none,tick label style={anchor={north east}}},
                        ytick={1.5,4},
                        yticklabels={$B_\mathsf{c1}$,$B_\mathsf{c2}$},
                        xtick style={draw=none},
                        ytick style={draw=none},
                        axis lines=middle,
                        hide obscured x ticks=false,
                        xmax=1.2,
                        ymin=0,
                        ymax=4.8,
                    ]
                    \addplot [color=mDarkTeal,name path=f,domain=0:1] {1.5*(1-x^2)};
                    \addplot [color=mDarkTeal,name path=g,domain=0:1] {4*(1-x^2)};
                    \path[name path=axis] (axis cs:0,0) -- (axis cs:1,0);
                    \addplot[color=mDarkTeal,opacity=0.3] fill between[of=f and axis];
                    \addplot[color=mLightGreen,opacity=0.3] fill between[of=f and g];
                    \node [anchor=center,align=center] at (axis cs:0.3,0.75) {Meißner-Phase};
                    \node [anchor=center,align=center] at (axis cs:0.3,2.5) {Shubnikov-Phase};
                    \node [anchor=center,align=center] at (axis cs:0.8,3.2) {Normalleiter};
                \end{axis}
            \end{tikzpicture}}
    \end{flushright}
\end{sframe}
\subsection{Shubnikov-Phase}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item magnetische "Flussschläuche" (Vortices)
                \item magnetischer Fluss in jedem Vortex
                      \begin{align*}
                          \Phi_0 & = \frac{h}{2e} \approx \SI{2e-15}{T m^2}
                      \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \centering
            \includegraphics[width=\linewidth]{fig/vortex.png}
        \end{column}
    \end{columns}
    \blfootnote{Bild: \fullcite{Wells:2015}}
\end{sframe}
\begin{sframe}
    \centering
    \textbf{\Large Wann gibt es die Shubnikov-Phase?}
    % \vspace*{2ex}
    %     \begin{itemize}
    %         \item Normalleiterzustand im Vortex kostet Energie
    %         \item wird durch Eindringen des Feldes kompensiert
    %     \end{itemize}
    % }
\end{sframe}
\subsection{Magnetische Eindringtiefe}
\begin{sframe}
    \centering
    \begin{tikzpicture}
        \pgfmathsetmacro{\xmin}{2}
        \pgfmathsetmacro{\xmax}{3}
        \begin{axis}[
                width=0.9\linewidth,
                height=0.8\textheight,
                domain=0:\xmax,
                ylabel=$B$,
                xmin=-\xmin, xmax=\xmax,
                ymin=0, ymax=1.2,
                xtick={0,1},
                xticklabels={0,$\lambda$},
                ytick={0,1},
                yticklabels={0,$B_0$},
            ]
            \addplot [thick,color=mDarkTeal,domain=-\xmax:0] {1};
            \addplot [thick,color=mDarkTeal,domain=0:\xmax] {exp(-x)};
            % \addplot [thick,color=gray] coordinates {(0,0) (0,2)};
            \path[name path=x0] (axis cs:0,2) -- (axis cs:\xmax,2);
            \path[name path=x1] (axis cs:0,0) -- (axis cs:\xmax,0);
            \addplot [fill=gray, fill opacity=0.3] fill between [of=x0 and x1];
            \node[anchor=center] at (axis cs:1.5,0.5) {$B_0\exp\left(-\frac{x}{\lambda}\right)$};
            \node[anchor=center] at (axis cs:1.5,1.1) {Supraleiter};
            \node[anchor=center] at (axis cs:-1,1.1) {Umgebung};
        \end{axis}
    \end{tikzpicture}
\end{sframe}
\subsection{Vortices}
\begin{sframe}
    \centering
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item $\lambda$: Abfall des Magnetfeldes
                \item $\xi$: charakteristische Länge des supraleitenden Zustands
                \item Typ II: $\kappa=\frac{\lambda}{\xi}>\frac{1}{\sqrt{2}}$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \includegraphics[width=\linewidth]{fig/vortexprofile.png}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Hirata:2012}}
\end{sframe}
\section{Theorie}
\subsection{Geschichte}
\begin{sframe}
    \begin{block}{BCS Theorie}
        \centering
        \begin{tabular}{c@{\hspace{4ex}}c@{\hspace{4ex}}c}
            \includegraphics[width=0.2\linewidth]{fig/Bardeen.jpg} &
            \includegraphics[width=0.2\linewidth]{fig/Cooper.jpg}  &
            \includegraphics[width=0.2\linewidth]{fig/Schrieffer.jpg} \\
            John                                                   &
            Leon                                                   &
            John Robert                                               \\
            \textbf{B}ardeen                                       &
            \textbf{C}ooper                                        &
            \textbf{S}chrieffer
        \end{tabular}

        Theorie 1957, Nobel Preis 1972
    \end{block}
    \blfootnote{Bild Schrieffer: Nationaal Archief und Spaarnestad Photo}
\end{sframe}
\subsection{BCS Theorie}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{itemize}
                \item metallisches Material
                \item Anziehungskraft zwischen Elektronen
                \item meist durch Gitterschwingungen (Phononen)
                \item Paare, die das Kondensat bilden
                \item Vorhersagen: Wärmekapazität, \dots
                \item anwendbar auf viele, aber nicht alle Materialien
            \end{itemize}
        \end{column}
        \begin{column}{0.3\textwidth}
            \centering
            \begin{tikzpicture}[
                    el/.style={
                            circle,
                            shading=ball,
                            minimum width=3ex,
                            ball color=mDarkTeal,
                        },
                ]
                \node[el] (e1) at (0,0) {};
                \node[el] (e2) at (0,2) {};
                \draw[dashed, opacity=0.3] (e1) -- (e2);
                \node[anchor=center] at (0,1) {$\propto\xi$};
            \end{tikzpicture}
        \end{column}
    \end{columns}
\end{sframe}
\subsection{Wärmekapazität -- \ce{Nb3Sn}}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \begin{itemize}
                \item nötige Energie, um die Temperatur zu erhöhen
                \item exponentieller Abfall
            \end{itemize}
        \end{column}
        \begin{column}{0.6\textwidth}
            \includegraphics[width=\linewidth]{fig/heat.png}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Jo:2014}}
\end{sframe}
\subsection{Isotopeneffekt -- Quecksilber}
\begin{sframe}
    \centering
    $\tc\propto\frac{1}{\sqrt{M}}$\\
    \vspace{2ex}
    \begin{tikzpicture}
        \pgfmathsetmacro{\xmin}{2}
        \pgfmathsetmacro{\xmax}{3}
        \begin{axis}[
                width=0.9\linewidth,
                height=0.6\textheight,
                ylabel={$\tc$ (K)},
                xlabel={$M$ (u)},
                xtick={200,201,202,203},
            ]
            \addplot[
                only marks,
                mark options={fill=mDarkTeal},
            ] coordinates {
                    (203.4, 4.126)
                    (202.0, 4.143)
                    (200.7, 4.150)
                    (199.7, 4.161)
                };
        \end{axis}
    \end{tikzpicture}
    \blfootnote{\fullcite{Reynolds:1950}}
\end{sframe}
\section{Materialien}
\subsection{Beispiele}
\begin{sframe}
    \centering
    \begin{tabular}{cccccc}
        \toprule
        Material   & Typ & {\tc{} (K)} & {$B_\mathsf{c(2)}$ (T)} & $\lambda$ (nm) & $\xi$ (nm) \\
        \midrule
        Blei       & I   & 7.2         & 0.08                    & 39             & 82         \\
        Aluminium  & I   & 1.18        & 10.5                    & 40             & 1510       \\
        NbTi       & II  & 9.6         & 10                      & 300            & 4          \\
        \ce{Nb3Sn} & II  & 18.1        & 22                      & 65             & 3          \\
        \ce{MgB2}  & II  & 39          & 16                      & 140            & 5          \\
        YBCO       & II  & 92          & {120 / 250}             & 170            & 1.8        \\
        % \multicolumn{4}{c}{\dots}                                 \\
        \bottomrule
    \end{tabular}

    \vspace{1ex}
    Flüssiger Stickstoff $\sim\SI{70}{K}$, darunter Helium notwendig
    \blfootnote{Bild: \fullcite{Poole:1995,Sekitani:2004}}
\end{sframe}
\subsection{NbTi}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item relativ günstig und einfach zu verarbeiten
                \item für viel Anwendungen ausreichend
                \item mit BCS erklärbar
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \includegraphics[width=\linewidth]{fig/nbti.jpg}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Zhang:2020}\\Bild: CERN}
\end{sframe}
\subsection{YBCO}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item \ce{YBa2Cu4O_{7-\delta}}
                \item hohe kritische Temperatur (mit Stickstoff erreichbar), Stromdichte und Felder
                \item aufwendige und teure Herstellung, wenig flexibel
                \item nicht mit BCS erklärbar
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \includegraphics[width=\linewidth]{fig/ybco.pdf}
        \end{column}
    \end{columns}
\end{sframe}
\section{Anwendungen}
\subsection{Kabel -- AmpaCity Essen}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Mittelspannungsnetz
                \item \SI{10}{kV}, \SI{2.3}{kA}
                \item Strecke $\sim\SI{1}{km}$
                \item Material: HTS
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \includegraphics[width=\linewidth]{fig/ampacity.jpg}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Stemmle:2013}\\
        Bild: ITEP/KIT}
\end{sframe}
\subsection{Magneten -- LHC}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.48\textwidth}
            \begin{itemize}
                \item Heliumkühlung $\sim\SI{1.9}{K}$
                \item NbTi ($\tc\approx\SI{9.6}{K}$, $B_\mathsf{c}\approx\SI{10}{T}$)
                \item 1232 Dipole mit\\$\sim\SI{8}{T}$, $\sim\SI{11}{kA}$
                \item halten Protonen auf Kreisbahn
            \end{itemize}
        \end{column}
        \begin{column}{0.52\textwidth}
            \includegraphics[width=\linewidth]{fig/cern.jpg}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Rossi:2003}\\
        Bild: CERN}
\end{sframe}
\subsection{Magneten -- MRT/MRI}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \only<1>{\begin{itemize}
                    \item Helium gekühlt
                    \item geschlossenes System\\
                          $\Rightarrow$ Helium muss nachgefüllt werden
                    \item typischer Weise\\
                          NbTi \SI{1.5}{T}
                    \item neuere Systeme\\
                          HTS \SI{3}{T}
                \end{itemize}}
        \end{column}
        \begin{column}{0.5\textwidth}
            \includegraphics[width=\linewidth]{fig/mri.jpg}
        \end{column}
    \end{columns}
    \blfootnote{\fullcite{Wang:2012a}}
\end{sframe}
\subsection{Spezialanwendungen -- SQUID}
\begin{sframe}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item sehr genaue Magnetfeld Messung
                \item basiert auf Quantisierung des mag. Flusses
                \item Josephson Kontakt:\\
                      schwacher Kontakt zwischen Supraleitern
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \includegraphics[width=\linewidth]{fig/SQUID.pdf}
        \end{column}
    \end{columns}
    \blfootnote{Bild: Wikimedia/Herbertweidner}
\end{sframe}
\subsection{Spezialanwendungen -- Strombegrenzer}
\begin{sframe}
    \centering
    \begin{circuitikz}
        \draw[
            thick,
            color=mDarkTeal
        ] (0,0) to[
            short,i=$I<I_\mathsf{c}$, color=mDarkTeal
        ] (2,0) to[
            color=mDarkTeal
        ] (2,1) to[
            R, l=Normalleiter,
            color=mDarkTeal
        ] (6,1) to[
            color=mDarkTeal
        ] (6,0) to[
            color=mDarkTeal
        ] (8,0);
        \draw[
            thick,
            color=TolDarkRed
        ] (2,0) to[
            R, *-*, l=Supraleiter,
            color=TolDarkRed
        ] (6,0);
    \end{circuitikz}

    \vspace{2ex}
    \visible<2>{
        \begin{circuitikz}
            \draw[
                thick,
                color=mDarkTeal
            ] (0,0) to[
                short,i=$I>I_\mathsf{c}$, color=mDarkTeal
            ] (2,0) to[
                color=TolDarkRed
            ] (2,1) to[
                R, l=\textcolor{TolDarkRed}{Normalleiter},
                color=TolDarkRed
            ] (6,1) to[
                color=TolDarkRed
            ] (6,0) to[
                color=mDarkTeal
            ] (8,0);
            \draw[
                thick,
                color=mDarkTeal
            ] (2,0) to[
                R, *-*, l=Supraleiter,
                color=mDarkTeal
            ] (6,0);
        \end{circuitikz}%
    }
    \blfootnote{\fullcite{Noe:2007}}
\end{sframe}
\subsection{Zusammenfassung}
\begin{sframe}
    \begin{itemize}
        \item $R=0$, $\chi=-1$ unterhalb kritischer Temperatur $\tc$
        \item Erklärung für "konventionelle" Supraleiter: BCS Theorie
        \item viele Metalle und andere Materialien
        \item Typ I vs. Typ II: Verhalten im Magnetfeld
        \item Anwendungen: \begin{itemize}
                  \item Ersatz für klassiche Leiter (Kabel, Magneten, \dots)
                  \item spezielle Anwendungen (Messgeräte, \dots)
              \end{itemize}
    \end{itemize}
\end{sframe}
\begin{frame}[standout]
    \Huge
    \vspace*{\fill}
    Danke für die Aufmerksamkeit!\\
    \vspace*{\fill}
    Fragen?
    \vspace*{\fill}
\end{frame}
\end{document}